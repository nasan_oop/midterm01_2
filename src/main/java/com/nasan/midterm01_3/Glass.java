/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nasan.midterm01_3;

/**
 *
 * @author nasan
 */
public class Glass {

    private int maxVol;//ตัวแปรกำหนดค่าสูงสุดที่ใส่น้ำได้(กำหนดปริมาตรของแก้ว)
    private int vol;//ตัวแปรกำหนดปริมาณน้ำที่จะใส่ไปในแก้ว
    private String name;//ตัวแปรเก็บชื่อแก้วน้ำ

    public Glass(int maxVol, String name) {
        this.maxVol = maxVol;
        this.name = name;
        System.out.println("This glass (" + this.name + ") maximum contain is " + this.maxVol + " ml.");//เมื่อ object ถูกสร้างขึ้น จะแสดงข้อความบอกว่าเก็บได้สูงสุดเท่าไหร่ และตอนนี้มีน้ำอยู่เท่าไหร่
    }

    public int getVol() {
        return vol;
    }

    public int getMaxVol() {
        return maxVol;
    }

    public void setVol(int vol) {
        if (vol > maxVol) { //ใช้ตรวจสอบ หากใส่ปริมาณน้ำมากกว่าที่แก้วรับได้ น้ำที่เกินมาจะล้น ปริมาณน้ำจะเท่ากับปริมาณสูงสุดที่น้ำจะเก็บได้
            this.vol = this.maxVol;
            System.out.println("this glass can contain only " + this.maxVol + " ml. ");
        } else {
            this.vol = vol;
        }
        System.out.println("This glass (" + this.name + ") maximum contain is " + this.maxVol + " ml, And now it's have " + this.vol + " ml.");
    }

    public int reduce(int volOfdrink) {//method สำหรับลดปริมาณน้ำในแก้ว โดยรับปริมาณน้ำที่จะลดลงไปจาก method drink ในคลาส Human
        return this.vol = this.vol - volOfdrink;
    }

    @Override
    public String toString() {//บอกปริมาณน้ำที่เหลือในแก้ว
        return "This glass (" + this.name + ") have " + vol + " ml. left";
    }
}
